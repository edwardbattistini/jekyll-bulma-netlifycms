# Jekyll-bulma-Netlifycms

An easy way to get started with the awesome jekyll + netlify suite.


*  [Bulma clean theme](https://github.com/chrisrhymes/bulma-clean-theme)
*  [Netlify CMS](https://www.netlifycms.org)

This is the best way IMO to power a simple website. 

Please feel free to contribute as I am not a profesional dev and there is a lot copy paste can't teach me.

Thanks